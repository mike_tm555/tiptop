class Store {
    constructor(data = {}) {
        this.data = data;
    }

    save(key, data) {
        this.data[key] = data;
    }

    delete(key, data) {
        const dataIndex = this.data[key].indexOf(data);
        this.data[key].splice(dataIndex, 1);
    }
}

let instance = null;

export function store(data) {
    if (instance === null) {
        instance = new Store(data);
    }

    return instance;
}


export function connect(selector) {
    return (component) => {
        return component.bind(component, {...selector()});
    }
}
