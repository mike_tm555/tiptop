export default class Session {
    read(key) {
        const savedItem = window.sessionStorage.getItem(key);

        return savedItem ? JSON.parse(savedItem) : null;
    }

    save(key, value) {
        window.sessionStorage.setItem(key, JSON.stringify(value));
    }

    delete(key) {
        window.sessionStorage.removeItem(key);
    }


    getAll() {
        const storage = {};

        Object.keys(window.sessionStorage).map((key) => {
            storage[key] = JSON.parse(window.sessionStorage[key]);
        });

        return storage;
    }
}
