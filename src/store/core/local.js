export default class Session {
    read(key) {
        const savedItem = window.localStorage.getItem(key);

        return savedItem ? JSON.parse(savedItem) : null;
    }

    save(key, value) {
        window.localStorage.setItem(key, JSON.stringify(value));
    }

    delete(key) {
        window.localStorage.removeItem(key);
    }


    getAll() {
        const storage = {};

        Object.keys(window.localStorage).map((key) => {
            storage[key] = JSON.parse(window.localStorage[key]);
        });

        return storage;
    }
}
