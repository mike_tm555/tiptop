import { store } from "./store";

export default class Model {
    host = 'http://localhost:3000/';

    path = '/';

    name = 'posts';

    constructor() {
        this.store = store().data;
    }

    get(id) {
        let post = null;

        if (this.store[this.name] !== undefined) {
            for (let key in this.store[this.name]) {
                if (this.store[this.name][key].id === id) {
                    post = this.store[this.name][key].id;

                    break;
                }
            }
        }

        if (!post) {
            return this.get(id);
        }

        return post;
    }

    async getAll() {
        if (this.store[this.name]) {
            return this.store[this.name];
        }

        return this.fetchAll();
    }

    async fetch(id) {
        return fetch(`${this.host}${this.path}/${id}`).then(response => {
            return response.json();
        }).then(json => {
            Object.assign(this.store, this.store, {
                    [this.name]: json.data
                }
            );

            return json.data;
        });
    }

    fetchAll() {
        this.store[this.name] = [];

        return fetch(`${this.host}${this.path}`).then(response => {
            return response.json();
        }).then(json => {
            this.store[this.name] = json.data;

            return this.store[this.name];
        });
    }

    delete(id) {

    }

    create(data) {

    }

    update(data) {

    }

    createQueryString = (params) => {
        let queryString = '';

        if (Object.keys(params)) {
            queryString = '?';

            const paramsMap = Object.keys(params).map((key) => {
                return `${key}=${params[key]}`;
            });

            queryString += paramsMap.join('&');
        }

        return queryString;
    }

    cloneData = (data) => {
        return JSON.parse(JSON.stringify(data));
    }
}
