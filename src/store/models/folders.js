import Model from "../core/model";

export default class FoldersModel extends Model {
    path = '/data/folders.json';

    name = 'folders';
}
