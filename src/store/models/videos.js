import Model from "../core/model";

export default class VideosModel extends Model {
    path = '/data/videos.json';

    name = 'videos';
}
