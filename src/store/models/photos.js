import Model from "../core/model";

export default class TemplatesModel extends Model {
    path = '/data/photos.json';

    name = 'photos';
}
