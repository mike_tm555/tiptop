import Model from "../core/model";

export default class FontsModel extends Model {
    host = 'https://www.googleapis.com/';

    path = 'webfonts/v1/webfonts';

    name = 'fonts';

    fetchAll() {
        this.store[this.name] = [];

        let queryString = this.createQueryString({
            key: "AIzaSyDuHJQbI2phSftOWmVagO1PVw-dNcD_M9s"
        });

        return fetch(`${this.host}${this.path}${queryString}`).then(response => {
                return response.json();
            }).then(json => {
                this.store[this.name] = json.items;

                return this.store[this.name];
            });
    }
}
