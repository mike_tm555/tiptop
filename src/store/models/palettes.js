import Model from "../core/model";

export default class PalettesModel extends Model {
    path = '/data/palettes.json';

    name = 'palettes';
}
