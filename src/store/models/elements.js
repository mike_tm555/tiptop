import Model from "../core/model";

export default class TemplatesModel extends Model {
    path = '/data/elements.json';

    name = 'elements';
}
