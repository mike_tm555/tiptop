import Model from "../core/model";

export default class TemplatesModel extends Model {
    path = '/data/templates.json';

    name = 'templates';
}
