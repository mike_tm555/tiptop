import React from 'react';
import {
    BrowserRouter,
    Switch,
    Route,
} from "react-router-dom";

import Home from './home';
import Logo from "./logo";
import Layout from "./layout";
import Plan from "./plan";

function Router() {
    return (
        <BrowserRouter>
            <Switch>
                <Route exact path="/">
                    <Home />
                </Route>
                <Route path="/logo/">
                    <Logo />
                </Route>
                <Route path="/layout/">
                    <Layout />
                </Route>
                <Route path="/plan/">
                    <Plan />
                </Route>
            </Switch>
        </BrowserRouter>
    )
}

export default React.memo(Router);
