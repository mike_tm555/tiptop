import React from 'react';
import Page from "../../components/page";
import Toolbar from "../../components/toolbar";
import Editor from "../../components/editor";
import ToolbarItems from "../../components/toolbar/items";

function Plan() {
    return (
        <Page className="plan-page">
            <Toolbar items={[
                ToolbarItems.photos,
                ToolbarItems.shapes,
                ToolbarItems.uploads,
            ]}/>

            <Editor/>
        </Page>
    )
}

export default React.memo(Plan);
