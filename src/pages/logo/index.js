import React from 'react';
import Page from "../../components/page";
import Toolbar from "../../components/toolbar";
import Editor from "../../components/editor";
import ToolbarItems from "../../components/toolbar/items";

function Logo() {
    return (
        <Page className="logo-page">
            <Toolbar items={[
                ToolbarItems.templates,
                ToolbarItems.elements,
                ToolbarItems.fonts,
                ToolbarItems.palettes,
                ToolbarItems.uploads,
            ]}/>

            <Editor/>
        </Page>
    )
}

export default React.memo(Logo);
