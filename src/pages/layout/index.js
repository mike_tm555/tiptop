import React from 'react';
import Page from "../../components/page";
import Toolbar from "../../components/toolbar";
import Editor from "../../components/editor";
import ToolbarItems from "../../components/toolbar/items";

function Layout() {
    return (
        <Page className="layout-page">
            <Toolbar items={[
                ToolbarItems.templates,
                ToolbarItems.photos,
                ToolbarItems.videos,
                ToolbarItems.elements,
                ToolbarItems.fonts,
                ToolbarItems.palettes,
                ToolbarItems.folders,
                ToolbarItems.uploads,
            ]}/>

            <Editor/>
        </Page>
    )
}

export default React.memo(Layout);
