import React from 'react';
import Page from "../../components/page";
import { Link } from "react-router-dom";
import Card from "../../components/card";
import './index.scss';

function Home() {
    return (
        <Page className="home-page">
            <h1>Select tool to start</h1>
            <div className="tools-wrapper">
                <Link to="/logo/"
                      className="tool-item">
                    <Card type="circle"
                          icon={<i className="fab fa-pied-piper"/>}
                          label={"LOGO"}
                    />
                </Link>
                <Link to="/layout/"
                      className="tool-item">
                    <Card type="circle"
                          icon={<i className="fa fa-pager"/>}
                          label={"LAYOUT"}
                    />
                </Link>
                <Link to="/plan/"
                      className="tool-item">
                    <Card type="circle"
                          icon={<i className="fas fa-layer-group"/>}
                          label={"PLAN"}
                    />
                </Link>
            </div>
        </Page>
    )
}

export default React.memo(Home);
