import React from 'react';
import './index.scss';

function Tooltip(props) {
    const positionX = props.positionX || "right";
    const positionY = props.positionX || "top";
    return (
        <div className={`tooltip-wrapper ${positionX} ${positionY}`}>
            {props.icon && (
                <div className="tooltip-icon">
                    {props.icon}
                </div>
            )}
            {props.children}
        </div>
    )
}

export default React.memo(Tooltip);
