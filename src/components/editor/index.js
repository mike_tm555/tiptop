import React from 'react';
import './index.scss';
import Button from "../button";

function Editor() {
    return (
        <div className="editor">
            <div className="editor-header">

            </div>
            <div className="editor-canvas">
                <canvas id="editor"/>
            </div>
            <div className="editor-footer">
                <Button className="editor-button"
                        type="icon">
                    <i className="fa fa-undo"/>
                </Button>
                <Button className="editor-button"
                        type="icon">
                    <i className="fa fa-redo"/>
                </Button>
                <Button className="editor-button"
                        type="icon">
                    <i className="fas fa-level-up-alt"/>
                </Button>
                <Button className="editor-button"
                        type="icon">
                    <i className="fas fa-level-down-alt"/>
                </Button>
                <Button className="editor-button"
                        type="icon">
                    <i className="fa fa-layer-group"/>
                </Button>
                <Button className="editor-button"
                        type="icon">
                    <i className="fa fa-trash"/>
                </Button>
                <Button className="editor-button"
                        type="icon">
                    <i className="fa fa-copy"/>
                </Button>
                <Button className="editor-button"
                        type="icon">
                    <i className="fa fa-object-group"/>
                </Button>
                <Button className="editor-button"
                        type="icon">
                    <i className="fa fa-object-ungroup"/>
                </Button>
                <Button className="editor-button"
                        type="icon">
                    <i className="fa fa-tint"/>
                </Button>
            </div>
        </div>
    )
}

export default React.memo(Editor);
