import React from 'react';
import './index.scss';

function Search(props) {
    return (
        <div className="search-box">
            <input type="search"
                   className="search-input"/>
        </div>
    )
}

export default React.memo(Search);
