import React from 'react';
import './index.scss';

function Sidebar(props) {
    return (
        <div className={"sidebar " + (props.direction || "left")}>
            {props.children}
        </div>
    )
}

export default React.memo(Sidebar);
