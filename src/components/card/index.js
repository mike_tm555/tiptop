import React from 'react';
import './index.scss';

function Card(props) {
    return (
        <div className={"card " + props.type || "circle"}>
            {props.icon && (
                <div className="card-icon">
                    {props.icon}
                </div>
            )}

            {props.label && (
                <div className="card-label">
                    {props.label}
                </div>
            )}
        </div>
    )
}

export default React.memo(Card);
