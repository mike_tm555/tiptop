import React from 'react';
import 'index.scss';

function Popup(props) {
    return (
        <div className="popup">
            {props.children}
        </div>
    )
}

export default React.memo(Popup);
