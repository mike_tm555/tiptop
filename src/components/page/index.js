import React from 'react';
import './index.scss';

function Page(props) {
    return (
        <div className={"page " + props.className}>
            {props.header && props.header}
            <div className="page-content">
                {props.children}
            </div>
            {props.footer && props.footer}
        </div>
    );
}

export default React.memo(Page);
