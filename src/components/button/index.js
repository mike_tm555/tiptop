import React from 'react';
import './index.scss';

function Button(props) {
    let buttonClass = `button`;

    if (props.type) {
        buttonClass += ` ${props.type}`;
    }

    if (props.className) {
        buttonClass += ` ${props.className}`;
    }


    return (
        <button className={buttonClass}>
            {props.children}
        </button>
    )
}

export default React.memo(Button);
