import React, { useState, useEffect } from 'react';
import Search from "../../../search";
import { connect } from "../../../../store/core/store";
import FontsModel from "../../../../store/models/fonts";
import Text from "./single";
import WebfontLoader from '@dr-kobros/react-webfont-loader';
import './index.scss';

function Texts(props) {
    const [ items, setItems ] = useState([]);
    const [ config, setConfig ] = useState(undefined);

    useEffect(() => {
        if(!items.length) {
            props.fonts.getAll().then((data) => {
                setItems(data);
            });
        } else if (!config) {
            setConfig({
                google: {
                    families: items.map(item => item.family)
                }
            });
        }
    });

    const callback = (status) => {
        console.log(status)
    };

    return config && (
        <div className="texts">
            <Search
                list={items}
                key="name"/>
            <WebfontLoader
                config={config}
                onStatus={callback}>
                <div className="texts-items">
                    <div className="row">
                        {items.map((item, key) => {
                            return (
                                <div key={key}
                                     className="col col-small-12 col-medium-12 col-large-12">
                                    <Text {...item}/>
                                </div>
                            )
                        })}
                    </div>
                </div>
            </WebfontLoader>
        </div>
    )
}


const mapStateToProps = () => {
    return {
        fonts: new FontsModel()
    };
};

export default React.memo(connect(mapStateToProps)(Texts));
