import React, {useEffect, useState} from 'react';

function Text(props) {
    const [style, setStyle] = useState({
        fontFamily: props.family,
        fontVariant: props.variants[0]
    });

    const changeVariant = (e) => {
        const newStyle = {
            fontFamily: props.family
        };

        const matches = e.target.value.match(/(\d+)/);

        let fontStyle = e.target.value;

        if (matches) {
            newStyle.fontWeight = matches[0];
            fontStyle = fontStyle.replace(matches[0], "");
        }

        if(fontStyle) {
            newStyle.fontStyle = fontStyle;
        }

        setStyle(newStyle);
    };

    return (
        <div className="text">
            <h4 className="text-title" style={style}>{props.family}</h4>
            {props.variants.length > 1 && (
                <select className="select-variant"
                        onChange={changeVariant}>
                    {props.variants.map((variant, key) => {
                        return (
                            <option key={key}
                                    value={variant}>
                                {`${props.family} - ${variant}`}
                            </option>
                        )
                    })}
                </select>
            )}
        </div>
    )
}

export default React.memo(Text);
