import React from "react";
import Templates from "./templates";
import Photos from "./photos";
import Videos from "./videos";
import Elements from "./elements";
import Texts from "./texts";
import Uploads from "./uploads";
import Shapes from "./shapes";
import Palettes from "./palettes";
import Folders from "./folders";

const ToolbarItems = {
    templates: {
        icon: (<i className="fas fa-poll-h"/>),
        label: 'Templates',
        component:  <Templates/>
    },
    photos: {
        icon: (<i className="fas fa-image"/>),
        label: 'Photos',
        component: <Photos/>
    },
    videos: {
        icon: (<i className="fas fa-photo-video"/>),
        label: 'videos',
        component: <Videos/>
    },
    elements: {
        icon: (<i className="fas fa-shapes"/>),
        label: 'Elements',
        component: <Elements/>
    },
    shapes: {
        icon: (<i className="fas fa-shapes"/>),
        label: 'Shapes',
        component: <Shapes/>
    },
    fonts: {
        icon: (<i className="fas fa-font"/>),
        label: 'Text',
        component: <Texts/>
    },
    palettes: {
        icon: (<i className="fas fa-palette"/>),
        label: 'palettes',
        component: <Palettes/>
    },
    folders: {
        icon: (<i className="fas fa-folder"/>),
        label: 'folders',
        component: <Folders/>
    },
    uploads: {
        icon: (<i className="fas fa-upload"/>),
        label: 'Uploads',
        component: <Uploads/>
    }
};

export default ToolbarItems;
