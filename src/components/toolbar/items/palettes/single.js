import React from 'react';
import Tooltip from "../../../tooltip";

function Palette(props) {
    return (
        <div className="palette">
            <div className="palette-box">
            </div>
            <Tooltip icon={<i className="fas fa-info"/>}/>
        </div>
    )
}

export default React.memo(Palette);
