import React, { useState, useEffect } from 'react';
import Search from "../../../search";
import { connect } from "../../../../store/core/store";
import PalettesModel from "../../../../store/models/palettes";
import Palette from "./single";
import './index.scss';

function Palettes(props) {
    const [ items, setItems ] = useState([]);

    useEffect(() => {
        if(!items.length) {
            props.palettes.getAll().then((data) => {
                setItems(data);
            });
        }
    });

    return (
        <div className="palettes">
            <Search
                list={items}
                key="name"/>
            <div className="palettes-items">
                <div className="row">
                    {items.map((item, key) => {
                        return (
                            <div key={key}
                                 className="col col-inline col-small-3 col-medium-3 col-large-3">
                                <Palette {...item}/>
                            </div>
                        )
                    })}
                </div>
            </div>
        </div>
    )
}


const mapStateToProps = () => {
    return {
        palettes: new PalettesModel()
    };
};

export default React.memo(connect(mapStateToProps)(Palettes));
