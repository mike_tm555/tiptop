import React, { useState, useEffect } from 'react';
import Search from "../../../search";
import { connect } from "../../../../store/core/store";
import ElementsModel from "../../../../store/models/elements";
import Element from "./single";
import './index.scss';

function Elements(props) {
    const [ items, setItems ] = useState([]);

    useEffect(() => {
        if(!items.length) {
            props.elements.getAll().then((data) => {
                setItems(data);
            });
        }
    });

    return (
        <div className="elements">
            <Search
                list={items}
                key="name"/>
            <div className="elements-items">
                <div className="row">
                    {items.map((item, key) => {
                        return (
                            <div key={key}
                                 className="col col-inline col-small-4 col-medium-4 col-large-4">
                                <Element {...item}/>
                            </div>
                        )
                    })}
                </div>
            </div>
        </div>
    )
}


const mapStateToProps = () => {
    return {
        elements: new ElementsModel()
    };
};

export default React.memo(connect(mapStateToProps)(Elements));
