import React from 'react';
import Tooltip from "../../../tooltip";

function Element(props) {
    return (
        <div className="element">
            <div className="element-image"
                 style={{backgroundImage: `url("${props.image}")`}}>
            </div>
            <Tooltip icon={<i className="fas fa-info"/>}/>
        </div>
    )
}

export default React.memo(Element);
