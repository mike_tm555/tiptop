import React from "react";
import Button from "../../../button";

function Create(props) {
    return (
        <div className="upload-file">
            <Button className="upload-file-button">
                {"Upload File"}
            </Button>
        </div>
    )
}

export default React.memo(Create);