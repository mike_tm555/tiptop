import React from 'react';
import './index.scss';
import Search from "../../../search";
import Create from "../uploads/create";

function Uploads() {
    return (
        <div className="uploads">
            <Search
                list={[]}
                key="name"/>
            <div className="uploads-items">
            </div>
            <Create items={[]}/>
        </div>
    )
}

export default React.memo(Uploads);
