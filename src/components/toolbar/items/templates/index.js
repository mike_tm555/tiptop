import React, { useState, useEffect } from 'react';
import Search from "../../../search";
import { connect } from "../../../../store/core/store";
import TemplatesModel from "../../../../store/models/templates";
import Template from "./single";
import './index.scss';

function Templates(props) {
    const [ items, setItems ] = useState([]);

    useEffect(() => {
        if(!items.length) {
            props.templates.getAll().then((data) => {
                setItems(data);
            });
        }
    });

    return (
        <div className="templates">
            <Search
                list={items}
                key="name"/>
            <div className="templates-items">
                <div className="row">
                    {items.map((item, key) => {
                        return (
                            <div key={key}
                                 className="col col-small-12 col-medium-12 col-large-6">
                                <Template {...item}/>
                            </div>
                        )
                    })}
                </div>
            </div>
        </div>
    )
}


const mapStateToProps = () => {
    return {
        templates: new TemplatesModel()
    };
};

export default React.memo(connect(mapStateToProps)(Templates));
