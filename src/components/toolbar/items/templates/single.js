import React from 'react';
import Tooltip from "../../../tooltip";

function Template(props) {
    return (
        <div className="template">
            <div className="template-source"
                 style={{backgroundImage: `url("${props.src}")`}}>
            </div>
            <Tooltip icon={<i className="fas fa-info"/>}/>
        </div>
    )
}

export default React.memo(Template);
