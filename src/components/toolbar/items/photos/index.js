import React, { useState, useEffect } from 'react';
import Search from "../../../search";
import { connect } from "../../../../store/core/store";
import PhotosModel from "../../../../store/models/photos";
import Photo from "./single";
import './index.scss';

function Photos(props) {
    const [ items, setItems ] = useState([]);

    useEffect(() => {
        if(!items.length) {
            props.photos.getAll().then((data) => {
                setItems(data);
            });
        }
    });

    return (
        <div className="photos">
            <Search
                list={items}
                key="name"/>
            <div className="photos-items">
                <div className="row">
                    {items.map((item, key) => {
                        return (
                            <div key={key}
                                 className="col col-inline col-small-12 col-medium-12 col-large-6">
                                <Photo {...item}/>
                            </div>
                        )
                    })}
                </div>
            </div>
        </div>
    )
}


const mapStateToProps = () => {
    return {
        photos: new PhotosModel()
    };
};

export default React.memo(connect(mapStateToProps)(Photos));
