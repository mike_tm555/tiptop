import React from 'react';
import Tooltip from "../../../tooltip";

function Photo(props) {
    return (
        <div className="photo">
            <img className="photo-source"
                 src={props.src}/>
            <Tooltip icon={<i className="fas fa-info"/>}/>
        </div>
    )
}

export default React.memo(Photo);
