import React, { useState, useEffect } from 'react';
import Search from "../../../search";
import { connect } from "../../../../store/core/store";
import VideosModel from "../../../../store/models/videos";
import Video from "./single";
import './index.scss';

function Videos(props) {
    const [ items, setItems ] = useState([]);

    useEffect(() => {
        if(!items.length) {
            props.videos.getAll().then((data) => {
                setItems(data);
            });
        }
    });

    return (
        <div className="videos">
            <Search
                list={items}
                key="name"/>
            <div className="videos-items">
                <div className="row">
                    {items.map((item, key) => {
                        return (
                            <div key={key}
                                 className="col col-inline col-small-12 col-medium-12 col-large-6">
                                <Video {...item}/>
                            </div>
                        )
                    })}
                </div>
            </div>
        </div>
    )
}


const mapStateToProps = () => {
    return {
        videos: new VideosModel()
    };
};

export default React.memo(connect(mapStateToProps)(Videos));
