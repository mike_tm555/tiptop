import React from 'react';
import Tooltip from "../../../tooltip";

function Video(props) {
    return (
        <div className="video">
            <img className="video-image"
                 src={props.image}/>
            <Tooltip icon={<i className="fas fa-info"/>}/>
        </div>
    )
}

export default React.memo(Video);
