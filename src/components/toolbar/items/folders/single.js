import React from 'react';
import Tooltip from "../../../tooltip";

function Folder(props) {
    return (
        <div className="folder">
            <div className="folder-box">
                <div className="folder-icon">
                    <i className="fas fa-folder"/>
                </div>
                <div className="folder-data">
                    <h4>{props.name}</h4>
                    <strong>{props.slug}</strong>
                </div>
            </div>
        </div>
    )
}

export default React.memo(Folder);
