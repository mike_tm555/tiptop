import React from "react";
import Folder from "./single";

function List(props) {
    return (
        <div className="list">
            {props.items.map((item, key) => {
                return (
                    <div className="list-item" key={key}>
                        <Folder {...item}/>
                        {item.children.length ? (
                            <List items={item.children}/>
                        ) : null}
                    </div>
                )
            })}
        </div>
    )
}

export default React.memo(List);