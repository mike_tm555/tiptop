import React from "react";
import Button from "../../../button";

function Create(props) {
    return (
        <div className="create-folder">
            <Button className="create-folder-button">
                {"Create Folder"}
            </Button>
        </div>
    )
}

export default React.memo(Create);