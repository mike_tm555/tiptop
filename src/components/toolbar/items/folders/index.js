import React, {useState, useEffect, useCallback} from 'react';
import Search from "../../../search";
import { connect } from "../../../../store/core/store";
import FoldersModel from "../../../../store/models/folders";
import List from "./list";
import Create from "./create";
import './index.scss';

function Folders(props) {
    const [ items, setItems ] = useState([]);

    const findChildren = (item, items) => {
        item.children = items.filter(element => {
            return (item.id === element.parentId);
        });

        if (item.children.length) {
            item.children.map((childItem) => {
                items.splice(items.indexOf(childItem), 1);
                return findChildren(childItem, items);
            })
        }

        return item;
    };

    useEffect(() => {
        if(!items.length) {
            props.folders.getAll().then((data) => {
                const clonedFolders = props.folders.cloneData(data);
                setItems(clonedFolders.map(item => {
                    return findChildren(item, clonedFolders);
                }));
            });
        }
    });

    return (
        <div className="folders">
            <Search
                list={items}
                key="name"/>
            <div className="folders-items">
                <List items={items}/>
            </div>
           <Create items={items}/>
        </div>
    )
}


const mapStateToProps = () => {
    return {
        folders: new FoldersModel()
    };
};

export default React.memo(connect(mapStateToProps)(Folders));