import React, {useState} from 'react';
import './index.scss';

function Toolbar(props) {
    const [activeTool, setActiveTool] = useState(props.items[0]);

    return (
        <div className={"toolbar " + (props.direction || "left")}>
            <div className="tools-navigation">
                {props.items.map((tool, key) => {
                    return (
                        <div key={key}
                             className={"tool-item " + (tool === activeTool && "active")}
                             onClick={
                                 () => setActiveTool(tool)
                             }>
                            {tool.icon && (
                                <span className="tool-icon">{tool.icon}</span>
                            )}
                            {tool.label && (
                                <span className="tool-name">{tool.label}</span>
                            )}
                        </div>
                    )
                })}
            </div>
            <div className="tools-container">
                {activeTool.component}
            </div>
        </div>
    )
}

export default React.memo(Toolbar);
