import React from 'react';
import './index.scss';

function Footer(props) {
    return (
        <div className="footer">
            {props.children}
        </div>
    );
}

export default React.memo(Footer);
