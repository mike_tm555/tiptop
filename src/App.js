import React from 'react';
import './App.scss';
import Router from "./pages";

function App() {
    return (
        <div className="App light-theme">
            <Router/>
        </div>
    );
}

export default App;
